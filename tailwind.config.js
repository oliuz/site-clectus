import scroll from 'tailwind-scrollbar'
import forms from "@tailwindcss/forms";
import querys from "@tailwindcss/container-queries";
import typography from "@tailwindcss/typography";
import aspectratio from "@tailwindcss/aspect-ratio";

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {},
    },

    plugins: [scroll, forms, querys, typography, aspectratio],
}