import './bootstrap';
import Alpine from 'alpinejs';
import Swal from 'sweetalert2';

window.addEventListener('alerta', (e) => {
	let data = e.detail
	console.log(data);
	Swal.fire({
		toast: data.toast,
		timerProgressBar: data.timerProgressBar,
		position: data.position,
		icon: data.type,
		iconColor: data.iconColor,
		iconHtml: data.iconHtml,
		width: data.width,
		title: data.title,
		html: data.html,
		customClass: data.customClass,
		background: data.background,
		color: data.color,
		showConfirmButton: false,
		timer: data.timer
	});
})
