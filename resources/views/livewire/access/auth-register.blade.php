<x-form wire:submit="regUser" class="bg-[#031b1b] w-[90%] lg:w-[35%] text-white italic">
	<div class="flex ">
		<span class="w-full text-2xl text-center">Site ...</span>
	</div>
	<hr class="mt-2 border-pink-600" />
	<x-label for="name" class="text-xl text-white" value="{{ __('Name') }}"/>
	<x-input  id="name" wire:model="name" autofocus type="text" autocomplete="name" :value="old('name')"/>
	<x-error :messages="$errors->get('name')" class="" />
	<x-label for="email" class="text-xl text-white" value="{{ __('Email') }}"/>
	<x-input id="email" wire:model="email" type="text" autocomplete="email" :value="old('email')"/>
	<x-error :messages="$errors->get('email')" class="" />
	<x-label for="password" class="text-xl text-white" value="{{ __('Password') }}"/>
	<x-input id="password" wire:model="password" type="password"/>
	<x-error :messages="$errors->get('password')" class="" />
	<x-label for="password_confirmation" class="text-xl text-white" value="{{ __('Confirm password') }}"/>
	<x-input id="password_confirmation" wire:model="password_confirmation" type="password"/>
	<x-error :messages="$errors->get('password_confirmation')" class="" />
	<div class="flex items-center flex-col lg:flex-row gap-2">
		<x-button class="w-full hover:bg-emerald-700 my-3 italic bg-[#120c2b] shadow shadow-black text-xl">
			{{ __('Register user') }}
		</x-button>
		<x-link class="p-2 !text-center text-white text-xl bg-orange-700 shadow shadow-black" href="{{ route('login') }}" active="" >
			{{ __('Login') }}
		</x-link>
	</div>
</x-form>





