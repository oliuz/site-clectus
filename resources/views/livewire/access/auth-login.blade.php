<x-form wire:submit="logUser" class="bg-[#031b1b] w-[90%] lg:w-[35%] text-white italic ">
	<div class="flex ">
		<span class="w-full text-2xl text-center">Site ...</span>
	</div>
	<hr class="mt-2 border-pink-600" />

	<x-label for="id_login" class="text-xl text-white" value="{{ __('Email, alias or phone') }}"/>
	<x-input id="id_login" wire:model="id_login" type="text" autofocus/>
	<x-error :messages="$errors->get('id_login')" class="text-xl mt3" />

	<x-label for="password" class="text-xl text-white" value="{{ __('Password') }}"/>
	<x-input id="password" wire:model="password" type="password"/>
	<x-error :messages="$errors->get('password')" class="text-xl mt3" />
		
	<div class="lg:flex-row flex items-center flex-col gap-2">
		<x-button wire:loading.attr="disabled" class="w-full hover:bg-emerald-900 my-3 italic bg-[#120c2b] shadow shadow-black text-xl">
			{{ __('Login') }}
		</x-button>
		<x-link  class="p-2  !text-center hover:bg-emerald-900 hover:text-white text-white text-xl bg-orange-700 shadow shadow-black" href="{{ route('register') }}" active="" >
			{{ __('Register') }}
		</x-link>
	</div>
	{{-- wire:loading wire:target="remove" pointer-events-none--}}
	<div wire:loading.delay wire:target="logUser" class="text-xl text-[#d6f334] mt-4">
		Procesando 🤔 ...
	</div>
	
</x-form>

{{-- <div class="fixed inset-0 flex items-center justify-center">
    <div class="w-3/4 p-6 text-green-600 bg-white rounded-lg">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur odio ex amet atque inventore architecto praesentium! Fuga iusto tenetur, architecto natus cum nisi ea, sit sapiente, expedita ducimus eos suscipit?
    </div>
</div> --}}
