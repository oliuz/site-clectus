<x-section>
	<article class="hidden col-span-12 p-2 shadow bg-neutral-900 lg:col-span-3 lg:grid shadow-black">
		<x-menu-panel>
	
		</x-menu-panel>
	</article>
	<article class="bg-neutral-900 h-[90vh] lg:h-[97vh] col-span-12 lg:col-span-9 shadow shadow-black p-2 flex flex-col gap-2 text-xl italic">
		<div class="p-2 bg-[#152122] shadow shadow-black flex flex-col justify-center text-center gap-1 lg:flex-row">
			<span class="block bg-[#2b2b14] shadow shadow-black p-2 w-full">
				{{ $this->article->id ? 'Actualizar artículo ...' : 'Crear artículo ...' }}
			</span>
		</div>
		<div class="p-3 overflow-y-scroll scrollbar-thin scrollbar-thumb-[#39ad35] scrollbar-track-red-800 shadow flex flex-col gap-2 shadow-black h-auto bg-violet-950">
			<x-form wire:submit="{{ $this->article->id ? 'update' : 'save' }}" class="bg-[#070f2b]">
				<div class="flex ">
					<span class="w-full text-2xl text-center">Fantomas ...</span>
				</div>
				<hr class="mt-2 border-pink-600" />
				<x-image-article wire:model="fileImg" :fileImg="$fileImg" :$article />
				<x-label for="title" class="text-xl text-orange-500" value="Título :" />
				<x-input id="title" wire:model="title" type="text" class="placeholder:text-yellow-400" placeholder="Escribe el título del artículo" autofocus/>
				<x-error :messages="$errors->get('title')" class="mt-2" />

				<x-label for="body" class="text-xl text-orange-500" value="{{ __('Description of article') }}" />
				<x-textarea placeholder="Escribe la descripción del artículo" id="body" wire:model="body"/>
				<x-error :messages="$errors->get('body')" class="mt-2" />

				<div class="flex flex-col gap-2 mt-2 lg:flex-row ">
					<x-button  wire:loading.attr="disabled" class="w-full shadow shadow-black italic">
						{{ $this->article->id ? 'Actualizar' : 'Crear' }}
					</x-button>
					<x-link wire:navigate href="{{ route('arts') }}" class="bg-blue-950 shadow !text-center shadow-black text-white ">
						{{ __('Cancel') }}
					</x-link>
				</div>
				<div wire:loading wire:target="save" class="text-xl text-[#d6f334] mt-4">
					Procesando 🤔 ...
				</div>
			</x-form>
		</div>
	</article>
</x-section>
