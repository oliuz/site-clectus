<x-section>
	<article class="hidden col-span-12 p-2 shadow bg-neutral-900 lg:col-span-3 lg:grid shadow-black">
		<x-menu-panel>
	
		</x-menu-panel>
	</article>
	<article class="bg-neutral-900 h-[90vh] lg:h-[97vh] col-span-12 lg:col-span-9 shadow shadow-black p-2">
		<div class="flex flex-col gap-1 h-[100%]">
			<div class="p-2 bg-[#152122] shadow shadow-black">
			@if ( session('msg') )
				<span class="bg-[#223baa98] p-2 border border-orange-700 block text-center">{{ session('msg') }}</span>
			@endif
			<span class="block p-1 text-xl italic text-center bg-green-900 shadow shadow-black">Perfil de : &nbsp; {{ Str::ucfirst(auth()->user()?->name)   }}</span>
			</div>
			<div class="p-3 overflow-y-scroll scrollbar-thin scrollbar-thumb-[#39ad35] scrollbar-track-red-800 shadow flex flex-col gap-2 shadow-black h-auto bg-violet-950">
				<x-form wire:submit="updateProfile" class="bg-[#1e0c3b] w-auto text-white italic">
				{{-- <x-image-user wire:model="fileImg" :fileImg="$fileImg" :$user /> --}}


				<div class="flex items-center flex-col lg:flex-row lg:gap-7 justify-center gap-3 shadow shadow-black p-4 bg-[#551e10] ">
					{{-- @php( $id = $attributes->wire('model')->value ) --}}
					@if ($fileImg)
						<img src="{{ $fileImg->temporaryUrl() }}" alt="Portada" class="bg-center bg-cover rounded-full shadow w-28 h-28 shadow-black" />
					@elseif($user->image)
						<div class="relative">
							<img src="{{ $user->image->url }}" alt="Portada" class="bg-center bg-cover rounded-full shadow w-28 h-28 shadow-black" />

							<x-button wire:click.prevent="delete" class="absolute left-0 -top-2 bg-[#160738] rounded-full">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 drop-shadow text-[#aa2]">
									<path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
								</svg>							  
							</x-button>
						</div>
					@else
						<img src="{{ asset('img/noimagen.jpeg') }}" alt="Portada" class="bg-center bg-cover rounded-full shadow w-28 h-28 shadow-black" />
					@endif
					<x-label for="img" class="p-2 px-4 text-xl text-white shadow cursor-pointer bg-teal-950 shadow-black" value="{{ __('Select the image jpg, png, svg or gif') }}" />
					<x-input wire:model="fileImg" type="file" class="hidden" id="img" />
					<x-error :messages="$errors->get('fileImg')" class="mt-3" />
				</div>


				<x-label for="name" class="text-xl text-white" value="{{ __('Name') }}"/>
				<x-input class="!text-white"  id="name" wire:model="name" autofocus type="text" autocomplete="name"  />
				<x-error :messages="$errors->get('name')" class="" />
				@if ( $user->alias)
					<x-label for="alias" class="text-xl text-white" value="{{ __('Alias') }}"/>
					<x-input id="alias" wire:model="alias" type="text" autocomplete="alias" />
					<x-error :messages="$errors->get('alias')" class="" />
				@else
					<x-label for="alias" class="text-xl text-white" value="{{ __('Alias') }}"/>
					<x-input placeholder="No hay alias" id="alias" wire:model="alias" type="text" autocomplete="alias" />
					<x-error :messages="$errors->get('alias')" class="" />
				@endif
				@if ($user->phone)
					<x-label for="phone" class="text-xl text-white" value="{{ __('Phone') }}"/>
					<x-input  id="phone" wire:model="phone" type="text" autocomplete="phone" />
					<x-error :messages="$errors->get('phone')" class="" />
				@else
					<x-label for="phone" class="text-xl text-white" value="{{ __('Phone') }}"/>
					<x-input placeholder="No hay phone" id="phone" wire:model="phone" type="text" autocomplete="phone" />
					<x-error :messages="$errors->get('phone')" class="" />
				@endif


				<x-label for="email" class="text-xl text-white" value="{{ __('Email') }}"/>
				<x-input id="email" wire:model="email" type="text" :value="old('email', $user)"/>
				<x-error :messages="$errors->get('email')" class="" />
				@if ($user->description)
					<x-label for="description" class="text-xl text-white" value="{{ __('Description') }}"/>
					<x-textarea autocomplete="description" id="description" wire:model="description"/>
					<x-error :messages="$errors->get('description')" class="" />
				@else
					<x-label for="description" class="text-xl text-white" value="{{ __('Description') }}"/>
					<x-textarea placeholder="No hay descripción" autocomplete="description" id="description" wire:model="description"/>
					<x-error :messages="$errors->get('description')" class="" />
				@endif
				<div class="flex items-center gap-2 flex-col lg:flex-row">
					<x-button class="w-full hover:bg-emerald-700 my-3 italic bg-[#120c2b] shadow shadow-black text-xl">
						{{ __('Update profile') }}
					</x-button>
					<x-link class="p-2 !text-center hover:bg-emerald-900 hover:text-white text-white text-xl bg-orange-700 shadow shadow-black" href="{{ route('panel') }}" active="" >
						{{ __('Cancel') }}
					</x-link>
				</div>
				</x-form>
				<div class="bg-red-600">
					<x-form wire:submit="updatePassword" class="bg-[#111016] w-auto text-white italic">
						<x-label for="current_password" class="text-xl text-white" value="{{ __('Current password') }}"/>
						<x-input id="current_password" wire:model="current_password" type="password" />
						<x-error :messages="$errors->get('current_password')" class="" />
						<x-label for="password" class="text-xl text-white" value="{{ __('New password') }}"/>
						<x-input type="password" id="password" wire:model="password"/>
						<x-error :messages="$errors->get('password')" class="" />
						<x-label for="password_confirmation" class="text-xl text-white" value="{{ __('Confirm password') }}"/>
						<x-input type="password" id="password_confirmation" wire:model="password_confirmation" />
						<x-error :messages="$errors->get('password_confirmation')"  />
		
						<x-button class="w-full hover:bg-emerald-700 my-3 italic bg-[#120c2b] shadow shadow-black text-xl">
							{{ __('Update password') }}
						</x-button>
					</x-form>
				</div>
				<div class="p-1 bg-orange-600">
					<x-form class="!p-0" wire:submit="deleteProfile">
						<x-button wire: class="w-full text-xl italic bg-red-800">
							{{ __('Delete account') }}
						</x-button>
					</x-form>
				</div>
			</div>
		</div>
	</article>
</x-section>
