<div x-data="{ open: true }">
    <x-dropdown align="left" width="80">
		<x-slot:trigger>
		<x-button class="!p-2 first-letter:uppercase shadow shadow-black  !bg-[#13041d] block w-full flex items-center gap-2 ">
			<div class="">	
				<img class="rounded-full shadow w-9 h-9 shadow-black" src="{{ $user->image ? $user->image?->url : asset('img/noimagen.jpeg') }}" alt="{{ $user->name }}" title="{{ $user->name }}">
			</div>
			<div class="flex ml-2 italic first-letter:uppercase text-white" x-data="{ name: '{{ Str::ucfirst(auth()->user()?->name)   }}' }" x-text="name" x-on:profile-updated.window="name = $event.detail.name"></div>
			
			<div class="ml-3">
				<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-orange-400">
					<path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
				</svg> 
			</div>
		</x-button>
		</x-slot>
		<div class="p-1 bg-black  !left-0 !rounded-none inset-0 flex flex-col gap-1">
			<x-button class="!p-0 italic">
				<x-link href="{{ route('profile') }}" class="text-white hover:text-amber-400 text-xl hover:bg-neutral-800">
					{{ __('Profile') }}
				</x-link>
			</x-button>
			<x-button class="!p-0 italic" wire:click="exit">
				<x-link class="text-white text-xl hover:text-amber-400 hover:bg-neutral-800">
					{{ __('Logout') }}
				</x-link>
			</x-button>
		</div>
	</x-dropdown>
</div>
