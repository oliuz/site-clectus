<x-section>
	<article class="hidden col-span-12 p-2 shadow bg-neutral-900 lg:col-span-3 lg:grid shadow-black">
		<x-menu-panel>
	
		</x-menu-panel>
	</article>
	<article class="bg-neutral-900 h-[90vh] lg:h-[97vh] col-span-12 lg:col-span-9 shadow shadow-black p-2 flex flex-col gap-2 text-xl italic">
		<div class="p-2 bg-[#1b8892] shadow shadow-black flex flex-col justify-center text-center gap-1 lg:flex-row">
			<span class="block bg-[#2b2b14] shadow shadow-black p-2 w-full">Listado de categorias</span>
			<x-link wire:navigate href="{{ route('create-cat') }}" active="" class="bg-blue-950 shadow !text-center shadow-black text-white lg:w-[30%] ">
				Crear categoria
			</x-link>
		</div>
		<div class="p-3 overflow-y-scroll scrollbar-thin scrollbar-thumb-[#39ad35] scrollbar-track-red-800 shadow flex flex-col gap-2 shadow-black h-auto bg-violet-950">
			
		<x-input-search wire:model.live="search" placeholder="Buscar categorias aca ..." />

		<div class="pt-4">
		@forelse ($categories as $category)
		<div wire:key="{{ $category->id }}" class="flex justify-between items-center p-2 my-2 shadow shadow-black flex-col lg:flex-row bg-[#080b2b]">
			<p class="pl-2">{{ $category->name }}</p>
			<div class="flex  items-center lg:flex-row text-center my-0">
				<x-link title="{{ $category->name }}" href="{{ route('edit-cat', $category) }}" class="bg-transparent hover:bg-transparent" >
					<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-orange-500">
						<path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125" />
					</svg>
				</x-link>
				<x-category-delete :$category :key="$category->id" />
			</div>
		</div>	
		@empty
		<div class="bg-teal-950 p-2 shadow shadow-black mt-2 text-center">
			No hay categorias
		</div>
		@endforelse
		</div>
			
		</div>
	</article>
</x-section>
