<x-section>
	<article class="hidden col-span-12 p-2 shadow bg-neutral-900 lg:col-span-3 lg:grid shadow-black">
		<x-menu-panel>
	
		</x-menu-panel>
	</article>
	<article class="bg-neutral-900 h-[90vh] lg:h-[97vh] col-span-12 lg:col-span-9 shadow shadow-black p-2 flex flex-col gap-2 text-xl italic">
		<div class="p-2 bg-[#1b8892] shadow shadow-black flex flex-col justify-center text-center gap-1 lg:flex-row">
			<span class="block bg-[#2b2b14] shadow shadow-black p-2 w-full">
				Listado de artículos
			</span>
			<x-link wire:navigate href="{{ route('create-art') }}" active="" class="bg-blue-950 shadow !text-center shadow-black text-white lg:w-[30%] ">
				Crear artículo
			</x-link>
		</div>
		<div class="p-3 h-auto overflow-y-scroll scrollbar-thin scrollbar-thumb-[#39ad35] scrollbar-track-red-800 shadow flex flex-col gap-2 shadow-black h-full bg-[#222]">
			<x-input-search wire:model.live="search" placeholder="Buscar artículos aca ..." />
			<div class="pt-4 flex gap-1 flex-col">
				@forelse ($articles as $article)
					<div x-data="{ open: false  }" class="relative italic ">
						<x-button @click="open = !open" aria-expanded="false" class="bg-[#09051f] shadow shadow-black text-white px-4 hover:brightness-110 italic focus:ring-0 flex items-center gap-2 block w-full" >
							<span class="mr-3">
								{{ $article->title }}
							</span>
							<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
								<path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
							</svg>						  
						</x-button>
						<div @keydown.escape.window="open = false" x-show="open" x-cloak x-trap="open" @click.outside="open = false" x-transition:enter=" ease-[cubic-bezier(.3,2.3,.6,1)] duration-200" x-transition:enter-start="!opacity-0 !mt-0" x-transition:enter-end="!opacity-1 !mt-3" x-transition:leave=" ease-out duration-200" x-transition:leave-start="!opacity-1 !mt-3" x-transition:leave-end="!opacity-0 !mt-0 [&_svg]:-rotate-[-90deg]" class="absolute bg-[#112] flex gap-2 flex-col lg:flex-row lg:justify-between items-center shadow-black shadow py-3 w-full lg:w-1/2 p-4 z-10 mt-2 left-0">
							<img class="rounded-full shadow w-9 h-9 shadow-black" src="{{ $article->image ? $article->image?->url : asset('img/noimagen.jpeg') }}" alt="{{ $article->title }}" title="{{ $article->title }}">
							{{ $article->body }}
							<div class="flex items-center">
								<x-link wire:navigate title="{{ $article->title }}" href="{{ route('edit-art', $article) }}" class="!bg-transparent !hover:bg-transparent border-none">
									<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-orange-500">
										<path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125" />
									</svg>
								</x-link>
								<x-article-delete :$article :key="$article->id" />
								<div wire:loading wire:target="update" class="text-xl text-[#d6f334] mt-4">
									Procesando 🤔 ...
								</div>
							</div>
						</div>
					</div>

				{{-- <div wire:key="{{ $articulo->id }}" class="flex justify-between items-center p-2 my-2 shadow shadow-black flex-col lg:flex-row bg-[#080b2b]">
					<p class="pl-2">{{ $category->name }}</p>
					<div class="flex  items-center lg:flex-row text-center my-0">
						<x-link title="{{ $category->name }}" href="{{ route('edit-cat', $category) }}" class="bg-transparent hover:bg-transparent" >
							<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-orange-500">
								<path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125" />
							</svg>
						</x-link>
						<x-category-delete :$category :key="$category->id" />
					</div>
				</div>	 --}}
				@empty
					<div class="bg-teal-950 p-2 shadow shadow-black mt-2 text-center">
						No hay artículos
					</div>
				@endforelse
			</div>
		</div>
	</article>
</x-section>
