<x-section>
	<article class="hidden col-span-12 p-2 shadow bg-neutral-900 lg:col-span-3 lg:grid shadow-black">
		<x-menu-panel>
	
		</x-menu-panel>
	</article>
	<article class="bg-neutral-900 h-[90vh] lg:h-[97vh] col-span-12 lg:col-span-9 shadow shadow-black p-2 flex flex-col gap-2 text-xl italic">
		<div class="p-2 bg-[#152122] shadow shadow-black flex flex-col justify-center text-center gap-1 lg:flex-row">
			<span class="block bg-[#2b2b14] shadow shadow-black p-2 w-full">
				{{ $this->category->id ? 'Actualizar categoria ...' : 'Crear categoria ...' }} 
			</span>
		</div>
		<div class="p-3 overflow-y-scroll scrollbar-thin scrollbar-thumb-[#39ad35] scrollbar-track-red-800 shadow flex flex-col gap-2 shadow-black h-auto bg-violet-950">
			<x-form wire:submit="{{ $this->category->id ? 'update' : 'save' }}" class="bg-[#070f2b]">
				<div class="flex ">
					<span class="w-full text-2xl text-center">Fantomas ...</span>
				</div>
				<hr class="mt-2 border-pink-600" />
				<x-label for="cat" class="text-xl text-orange-500" value="Nombre :" />
				<x-input id="cat" wire:model="name" type="text" class="placeholder:text-yellow-400" placeholder="Escribe el nombre de la categoria" autofocus/>
				<x-error :messages="$errors->get('name')" class="mt-2" />
				<div class="flex flex-col gap-2 mt-2 lg:flex-row ">
					<x-button  wire:loading.attr="disabled" class="w-full shadow shadow-black italic">
						{{ $this->category->id ? 'Actualizar' : 'Crear' }} 
					</x-button>
					<x-link wire:navigate href="{{ route('cats') }}" active="" class="bg-blue-950 shadow !text-center shadow-black text-white ">
						{{ __('Cancel') }}
					</x-link>
				</div>
				<div wire:loading wire:target="save" class="text-xl text-[#d6f334] mt-4">
					Procesando 🤔 ...
				</div>
			</x-form>
		</div>
	</article>
</x-section>
