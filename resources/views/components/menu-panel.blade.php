<nav class=" flex flex-col gap-1 italic">
	@livewire('panel.menu-user')
	<x-link class="p-2.5 bg-[#310617] text-white shadow shadow-black text-xl" wire:navigate href="{{ route('cats') }}" >
		Categorias
	</x-link>
	<x-link class="p-2.5 bg-[#310617] text-white shadow shadow-black text-xl" wire:navigate href="{{ route('arts') }}" >
		Artículos
	</x-link>
</nav>