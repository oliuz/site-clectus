<form {{ $attributes->class([' p-4 shadow shadow-black text-center']) }}>
	{{ $slot }}
</form>