@props(['value'])

<label {{ $attributes->merge(['class' => 'block  font-medium text-sm text-gray-700 mt-3 md:my-4']) }}>
    {{ $value ?? $slot }}
</label>
