@props(['name'])

<div
    x-data="{ modalShow : false , name : '{{ $name }}'}"
    x-show=" modalShow "
    x-on:show-modal.window=" modalShow = ($event.detail.name === name) "
    x-on:close-modal.window=" modalShow = false "
    x-on:keydown.escape.window=" modalShow = false "
    style="display: none;"
    class="fixed inset-0 z-50 mx-auto "
	x-transition:enter="ease-out duration-300"
    x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
    x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
    x-transition:leave="ease-in duration-200"
    x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
    x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
>
    {{-- Fondo modal --}}
    <div 
		x-on:click=" modalShow = false " 
		class="fixed inset-0 bg-[#2875665d] opacity-50"
	>
	</div>
    {{-- Ventana Modal  --}}
    <div {{ $attributes->class(['mt-6 overflow-hidden transition-all transform bg-white w-[97%] md:w-[40%] mx-auto shadow shadow-black p-1']) }}>
        {{ $slot }}
    </div>
    {{-- Ventana Modal  --}}
</div>