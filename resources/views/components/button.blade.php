<button {{ $attributes->merge(['type' => 'submit', 'class' => 'p-2 bg-gray-800  tracking-widest text-center transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
