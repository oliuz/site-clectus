<div class="flex items-center flex-col lg:flex-row lg:gap-7 justify-center gap-3 shadow shadow-black p-4 bg-[#551e10] ">
	@php( $id = $attributes->wire('model')->value )
	@if ($fileImg)
		<img src="{{ $fileImg->temporaryUrl() }}" alt="Portada" class="bg-center bg-cover rounded-full shadow w-28 h-28 shadow-black" />

	@elseif($article->image)
		<div class="relative">
			<img src="{{ $article->image->url }}" alt="Portada" class="bg-center bg-cover rounded-full shadow w-28 h-28 shadow-black" />
			<x-button wire:click.prevent="delete" class="absolute left-0 -top-2 bg-[#160738] rounded-full">
				<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 drop-shadow text-[#aa2]">
					<path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
				</svg>							  
			</x-button>
		</div>
	@else
		<img src="{{ asset('img/noimagen.jpeg') }}" alt="Portada" class="bg-center bg-cover rounded-full shadow w-28 h-28 shadow-black" />
	@endif
	<x-label for="{{ $id }}" class="p-2 px-4 text-xl text-white shadow cursor-pointer bg-teal-950 shadow-black" value="{{ __('Select the image jpg, png, svg or gif') }}" />
	<x-input wire:model="{{ $id }}" type="file" class="hidden" id="{{ $id }}" />
	<x-error :messages="$errors->get('fileImg')" class="mt-3" />
</div>