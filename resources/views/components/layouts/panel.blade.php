<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>{{ $title ?? 'Page Title' }}</title>
		<link rel="icon" href="{{ asset('img/krull.jpg') }}" type="image/x-icon">
		@livewireStyles
		@vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="bg-[#412916] overflow-hidden relative">
        {{ $slot }}
		@livewireScripts
		<x-modal-base name="side-bar" class="!bg-[#ca5f17] text-red-400 w-[75%] lg:hidden h-screen -mt-0 top-0 -mx-0">
			<x-menu-panel>
	
			</x-menu-panel>
		</x-modal-base>
		<x-button class="absolute right-9 bottom-14 z-50 p-3 shadow rounded-full shadow-black bg-green-900 lg:hidden " x-data @click.prevent="$dispatch('show-modal', { name : 'side-bar' })">
			<svg xmlns="http://www.w3.org/2000/svg" class="h-9 w-9 text-orange-600" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
				<path stroke-linecap="round" stroke-linejoin="round" d="M4 6h16M4 12h16M4 18h16" />
			  </svg>
		</x-button>
    </body>
</html>
