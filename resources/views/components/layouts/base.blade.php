<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>{{ $title ?? 'Page Title' }}</title>
		@livewireStyles
		@vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="fixed inset-0 flex items-center justify-center bg-rose-950">
        {{ $slot }}
		@livewireScripts
    </body>
</html>
