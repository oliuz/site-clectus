<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>{{ $title ?? 'Page Title' }}</title>
		@livewireStyles
		@vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="bg-zinc-900 italic text-white text-xl">
        <h1>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et blanditiis doloribus necessitatibus repudiandae, natus dolores voluptatem amet, cupiditate velit asperiores deserunt. Earum, id! Officia, asperiores placeat fugiat sint sed earum!</h1>
		@livewireScripts
    </body>
</html>