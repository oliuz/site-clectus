<?php

use App\Livewire\Access\AuthLogin;
use App\Livewire\Access\AuthRegister;
use App\Livewire\Panel\ArticleForm;
use App\Livewire\Panel\Articles;
use App\Livewire\Panel\Categories;
use App\Livewire\Panel\CategoryForm;
use App\Livewire\Panel\PanelBase;
use App\Livewire\Panel\ProfileUser;
use App\Livewire\Site;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::view('/', 'blog')->name('blog');

Route::get('/', Site::class)->name('site');

Route::get('site-register', AuthRegister::class)->name('register');
Route::get('site-login', AuthLogin::class)->name('login');


Route::middleware('auth')->prefix('tablero')->group(function () {

	// Rutas Panel
	Route::get('', PanelBase::class)->name('panel');
	Route::get('profile', ProfileUser::class)->name('profile');

	// Rutas Categorias
	Route::get('categorias', Categories::class)->name('cats');
	Route::get('crear/categoria', CategoryForm::class)->name('create-cat');
	Route::get('editar/{category:id}', CategoryForm::class)->name('edit-cat');

	// Rutas Artículos
	Route::get('articulos', Articles::class)->name('arts');
	Route::get('crear/articulo', ArticleForm::class)->name('create-art');
	Route::get('{article:id}/editar', ArticleForm::class)->name('edit-art');
});