<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;
use Livewire\WithFileUploads;

trait ImageImgurApi
{
    use WithFileUploads;
	
	public function imageApi( $model, $image ): void {
		$response = Http::withHeaders(['Authorization' => 'Client-ID' . env('IMGUR_ID')])
		->attach('image', file_get_contents($this->fileImg->getRealPath()), $this->fileImg->getClientOriginalName())
		->post('https://api.imgur.com/3/image');

		$data = $response->json();
		// dd($data);
		$model->image()->updateOrCreate([
			'url' => $data['data']['link'],
            'imageDeleteHash' => $data['data']['deletehash'],
            'imageId' => $data['data']['id']
		]);
	}

	public function apiDeleteImgur($model, $imageDeleteHash): void {
        Http::withHeaders(['Authorization' => 'Client-ID '. env('IMGUR_ID')])
            ->delete('https://api.imgur.com/3/image/'.$imageDeleteHash);

        $model->image()->delete();
    }
}
