<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;
use Livewire\WithFileUploads;

trait ImageImgbbApi
{
    use WithFileUploads;

	public function imageApi( $model, $image ): void {
		$response = Http::attach('image', file_get_contents($image->getRealPath()), $image->getClientOriginalName())
		->post('https://api.imgbb.com/1/upload?key='. env('IMGBB_SECRET'));

		$data = $response->json();
		// dd($data);
		$model->image()->updateOrCreate([
			'url' => $data['data']['url'],
            'imageDeleteHash' => $data['data']['delete_url'],
            'imageId' => $data['data']['id']
		]);
	}

	public function apiDeleteImgur($model, $imageDeleteHash): void {
        Http::delete('https://api.imgbb.com/1/upload'.$imageDeleteHash);

        $model->image()->delete();
    }
}
