<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Article extends Model
{
    use HasFactory, Sluggable;

	public function sluggable(): array {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

	protected function title(): Attribute {
		return Attribute::make(
			set: function($val){
				return strtolower($val);
			},
			get: function($val){
				return ucfirst($val);
			}
		);
	}

	protected function body(): Attribute {
		return Attribute::make(
			set: function($val){
				return strtolower($val);
			},
			get: function($val){
				return ucfirst($val);
			}
		);
	}
	
    public function getRouteKeyName() {
        return 'slug';
    }

	public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
