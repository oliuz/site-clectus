<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory, Sluggable;

	public function sluggable(): array
	{
		return [
			'slug' => [
                'source' => 'name'
            ]
		];
	}

	public function getRouteKeyName() {
		return 'slug';
	}
	
	protected function name(): Attribute {
		return Attribute::make(
			set: function($val){
				return strtolower($val);
			},
			get: function($val){
				return ucfirst($val);
			}
		);
	}
}