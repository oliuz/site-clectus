<?php

namespace App\Livewire;

use Livewire\Attributes\Layout;
use Livewire\Attributes\Title;
use Livewire\Component;

use function PHPSTORM_META\type;

#[Layout('components.site.site-base')]
class Site extends Component
{
	public function alert(): void
	{
		$this->dispatch(
			'alerta',
			type: 'success',
			title: 'Like ::::....',
			background: '#1f0d2e',
			color: 'red',
			position: 'center',
			timer: 3500
		);
	}

	#[Title('Site ::...')]
    public function render()
    {
        return view('livewire.site');
    }
}
