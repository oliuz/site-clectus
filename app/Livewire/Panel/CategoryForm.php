<?php

namespace App\Livewire\Panel;

use App\Models\Category;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Livewire\Attributes\Layout;
use Livewire\Attributes\Title;
use Livewire\Attributes\Validate;
use Livewire\Component;

#[Layout('components.layouts.panel')]
class CategoryForm extends Component
{
	public ?Category $category;

	#[Validate]
	public string $name;

	// #[Locked]
	#[Validate]
	public string $slug;

	public function rules(): array {
		return [
			'name'  => 'required|min:3|max:70',
			// 'slug' => [
            //     'alpha_dash',
			// 	'required',
            //     Rule::unique(Category::class)->ignore($this->category),
            // ],
		];
	}

	public function mount(Category $category) {
		$this->category = $category;
		if ( $this->category->exists ) {
			$this->fill($this->category);
		}
	}

    public function updatedName() {
        $this->slug = SlugService::createSlug(Category::class, 'slug', $this->name);
    }

	public function save() {
		$this->validate();
		if ($this->category->exists ) {
			$this->update();
		} else {
			$this->category = Category::create([
				'name' => $this->name,
				'slug' => $this->slug
			]);
		}
		// $this->category->create([
		// 	'name' => $this->name
		// ]);
		// $this->dispatch(
		// 	'alerta',
		// 	timerProgressBar: true,
		// 	toast: true,
		// 	iconColor: '#ec4706',
		// 	html: "<h1>Categoria creada con exito !!!.</h1>",
		// 	background: '#454757',
		// 	iconHtml: '
		// 		<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
		// 			<path stroke-linecap="round" stroke-linejoin="round" d="m4.5 12.75 6 6 9-13.5" />
		// 		</svg>
		  
		// 	  ',
		// 	color: 'yellow',
		// 	position: 'top',
		// 	timer: 4500
		// );
		return to_route('cats');
	}


	public function update() {
		$this->validate();
		$this->category->update([
			'name' => $this->name,
			'slug' => $this->slug
		]);
		return to_route('cats');
	}


	#[Title('Crear Categoria ..')]
    public function render()
    {
        return view('livewire.panel.category-form');
    }
}
