<?php

namespace App\Livewire\Panel;

use Livewire\Attributes\Layout;
use Livewire\Attributes\Title;
use Livewire\Component;

#[Layout('components.layouts.panel')]
class PanelBase extends Component
{
	#[Title('Panel ::.')]
    public function render()
    {
        return view('livewire.panel.panel-base');
    }
}
