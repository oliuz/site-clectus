<?php

namespace App\Livewire\Panel;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class MenuUser extends Component
{
	public ?User $user;

	public function mount() {
		$this->user = auth()->user();
	}

	public function exit(): mixed
	{
		Auth::logout();
		session()->invalidate();
        session()->regenerateToken();
		$this->dispatch(
			'alerta',
			type: 'success',
			title: 'Cerrando Sesión ...',
			background: '#1f0d2e',
			color: 'yellow',
			position: 'top',
			timer: 4500
		);
		return to_route('login');
	}


    public function render()
    {
        return view('livewire.panel.menu-user');
    }
}
