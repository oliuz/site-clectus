<?php

namespace App\Livewire\Panel;

use App\Models\User;
use App\Traits\ImageImgurApi;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;
use Livewire\Attributes\Layout;
use Livewire\Attributes\Title;
use Livewire\Attributes\Validate;
use Livewire\Component;

#[Layout('components.layouts.panel')]
class ProfileUser extends Component
{	
	use ImageImgurApi;
	public User $user;
	
	#[Validate]
	public string  $name;
	public ?string $alias;
	public ?string $phone;
	public string  $email;
	public ?string $description;
	public $fileImg;

	protected function rules(): array {
		return [
			'fileImg' => [
                'nullable',
                'image',
                'mimes:jpeg,png,jpg,gif,svg',
                'max:2048',
            ],
			// 'name'  => 'min:3',
			// 'alias' => [
            //     'nullable',
			// 	'min:3',
            //     Rule::unique('users', 'alias')->ignore($this->user),
            // ],
			// 'phone' => [
            //     'nullable',
			// 	'min:10',
            //     Rule::unique('users', 'phone')->ignore($this->user),
            // ],
			// 'email' => [
            //     Rule::unique('users', 'email')->ignore($this->user),
            // ],
			// 'description'  => 'nullable|min:7',
		];
	}

	public function mount() {
		$this->user = auth()->user();
		$this->fill($this->user);
	}


	public function updateProfile() {
		// dd($this->user);
		$this->validate();
		if ( $this->fileImg ) {
			$this->imageApi($this->user, $this->fileImg);
			// dd($this->imageApi($this->user, $this->fileImg));
			$this->cleanImage();
		}
		// $this->user->update([
		// 	'name' => $this->name,
		// 	'alias' => $this->alias,
		// 	'phone' => $this->phone,
		// 	'email' => $this->email,
		// 	'description' => $this->description
		// ]);
		
		$this->redirect(route('profile'), navigate: true);
	}

    public function cleanImage(): void {
        $fileOld = Storage::files('livewire-tmp');

        foreach ($fileOld as $file) {
            Storage::delete($file);
        }
    }

	public function delete() {
        $this->apiDeleteImgur($this->user, $this->user->image->imageDeleteHash);
		$this->dispatch(
			'alerta',
			type: 'success',
			title: 'Foto borrada con éxito',
			background: '#1f0d2e',
			color: 'yellow',
			position: 'top',
			timer: 3500
		);
		$this->redirect(route('profile'), navigate: true);
    }

	public function updatePassword(): void
	{
		try {
			$validatepass = $this->validate([
                'current_password' => 'required|string|current_password',
                'password' => ['required', 'string', Password::min(2), 'confirmed']
            ]);
		} catch (ValidationException $e) {
			$this->fill([
                'current_password' => '',
                'password' => '',
                'password_confirmation' => '',
            ]);
            throw $e;
		}

		auth()->user()->update([
            'password' => bcrypt($validatepass['password']),
        ]);
		$this->dispatch(
			'alerta',
			type: 'success',
			title: 'Contraseña actualizada con éxito',
			background: '#1f0d2e',
			color: 'yellow',
			position: 'top',
			timer: 3500
		);

        $this->fill([
            'current_password' => '',
            'password' => '',
            'password_confirmation' => '',
        ]);
	}

	public function deleteProfile(): mixed
	{
		tap(auth()->user(), fn () => auth()->logout())->delete();

        session()->invalidate();
        session()->regenerateToken();
		$this->dispatch(
			'alerta',
			type: 'success',
			title: 'Borrando cuenta ....',
			background: '#1f0d2e',
			color: 'yellow',
			position: 'top',
			timer: 3500
		);
        return to_route('login');
	}

	#[Title('Actualizar perfil ::')]
    public function render()
    {
        return view('livewire.panel.profile-user');
    }
}
