<?php

namespace App\Livewire\Panel;

use App\Models\Category;
use Livewire\Attributes\Layout;
use Livewire\Attributes\Title;
use Livewire\Component;

#[Layout('components.layouts.panel')]
class Categories extends Component
{
	public $search = '';

	// public function mount(Category $category) {
	// 	$this->category = $category;
	// SET FOREIGN_KEY_CHECKS = 0;
	// TRUNCATE site.categories;
	// SELECT * FROM site.categories;
	// }

	public function delete($id) {
		Category::findOrFail($id)->delete();
        $this->redirect(route('cats'), navigate: true);
	}

	#[Title('Categorias ...')]
    public function render()
    {
        return view('livewire.panel.categories', [
			'categories' => Category::query()
			->when( 
				!empty(trim($this->search)),
				fn($q) => $q->where('name', 'like', "%{$this->search}%")
			)->orderByDesc('created_at')->get()
			// ->latest()
	
		]);
    }
}
