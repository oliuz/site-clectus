<?php

namespace App\Livewire\Panel;

use App\Models\Article;
use App\Traits\ImageImgbbApi;
use Livewire\Attributes\Layout;
use Livewire\Attributes\Title;
use Livewire\Component;


#[Layout('components.layouts.panel')]
class Articles extends Component
{
    use ImageImgbbApi;

    public $search = '';

    public function delete($id): void
    {
        $article = Article::findOrFail($id);
        $this->apiDeleteImgur($article, $article->image->imageDeleteHash);

        $article->delete();
        $this->redirect(route('arts'), navigate: true);
    }

    #[Title('Artículos ...')]
    public function render()
    {
        return view('livewire.panel.articles', [
            'articles' => Article::query()
                ->when(
                    !empty(trim($this->search)),
                    fn($q) => $q->where('title', 'like', "%{$this->search}%")
                )->orderByDesc('created_at')->get()
        ]);
    }
}
