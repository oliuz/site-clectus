<?php

namespace App\Livewire\Panel;

use App\Models\Article;
use App\Models\User;
use App\Traits\ImageImgbbApi;
use App\Traits\ImageImgurApi;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Attributes\Layout;
use Livewire\Attributes\Title;
use Livewire\Attributes\Validate;
use Livewire\Component;

#[Layout('components.layouts.panel')]
class ArticleForm extends Component
{
    use ImageImgbbApi;

    public Article $article;

    #[Validate()]
    public string $title;
    public string $body;
    public ?string $slug;
    public $fileImg;

    public function rules(): array
    {
        return [
            'fileImg' => [
                'nullable',
                'image',
                'mimes:jpeg,png,jpg,gif,svg',
                'max:2048',
            ],
            'title' => 'required|min:3',
            'body' => 'required|min:7',
        ];
    }

    public function mount(Article $article): void
    {
        $this->article = $article;
        if ($this->article->exists) {
            $this->fill($this->article);
        }
    }

    public function updatedTitle(): void
    {
        $this->slug = SlugService::createSlug(Article::class, 'slug', $this->title);
    }

    public function save(): void
    {
        $this->validate();

        if ($this->article->exists) {
            $this->update();
        } else {
            $article = $this->article->create([
                'title' => $this->title,
                'slug' => $this->slug,
                'body' => $this->body,
                'user_id' => auth()->id()
            ]);

            if ($this->fileImg) {
                $this->imageApi($article, $this->fileImg);

                $this->cleanImage();
            }
        }

        $this->redirect(route('arts'), navigate: true);
    }

    public function update(): void
    {
        $this->validate();
        if ($this->fileImg) {
            $this->imageApi($this->article, $this->fileImg);
            $this->cleanImage();
        }
        $this->article->update([
            'title' => $this->title,
            'slug' => $this->slug,
            'body' => $this->body
        ]);

        $this->redirect(route('arts'), navigate: true);
    }

    public function delete(): void
    {
        $this->apiDeleteImgur($this->article, $this->article->image->imageDeleteHash);

        // return to_route('user.edit', $this->user)->with('status', 'Datos actualizados exitosamente');
        $this->redirect(route('edit-art', $this->article), navigate: true);
    }

    public function cleanImage(): void
    {
        $fileOld = Storage::files('livewire-tmp');
        foreach ($fileOld as $file) {
            Storage::delete($file);
        }
    }

    #[Title('Crear Artículo ..')]
    public function render()
    {
        return view('livewire.panel.article-form');
    }
}
