<?php

namespace App\Livewire\Access;

use App\Models\User;
use Livewire\Attributes\Title;
use Livewire\Attributes\Validate;
use Livewire\Component;

class AuthRegister extends Component
{
	public User $user;
	#[Validate]
	public string $password = '', $name = '', $email = '', $password_confirmation = '';

	public function rules(): array
	{
		return [
			'name'       => 'required|min:3',
			'email'      => 'required|string|email|unique:users,email',
			'password'   => 'required|min:2|confirmed'
		];
	}

	public function mount(User $user)
	{
		$this->user = $user;
	}

	public function regUser()
	{
		$dataForm = $this->validate();
		User::create($dataForm);
		// $this->dispatch('close-modal', name : 'authReg');
		$this->dispatch(
			'alerta',
			type: 'success',
			title: 'Usuario registrado con éxito',
			background: '#1f0d2e',
			color: 'yellow',
			position: 'top',
			timer: 3500
		);
		return to_route('login');
	}
	#[Title('Registro ::')]
    public function render()
    {
        return view('livewire.access.auth-register');
    }
}
