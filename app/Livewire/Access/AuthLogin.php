<?php

namespace App\Livewire\Access;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Title;
use Livewire\Component;

class AuthLogin extends Component
{
	public string $password = '', $id_login;
    public bool $remember = false;


	public function mount(User $user): void {
		$this->user = $user;
	}


	public function logUser() {
		$this->validate([
            'id_login'  => 'required|string',
            'password'  => 'required'
        ]);

		$credentials = [
            filter_var($this->id_login, FILTER_VALIDATE_EMAIL) ? 'email' : 
            (is_numeric($this->id_login) ? 'phone' : 'alias') => $this->id_login,
            'password'  => $this->password
        ];

		if (Auth::attempt($credentials, $this->remember)) {
			session()->regenerate();
            $user = Auth::getProvider()->retrieveByCredentials($credentials);
			Auth::login($user, $this->remember = true);
			return to_route('panel')->with('msg', __('Session started'));
		} else {
			$this->dispatch(
				'alerta',
				timerProgressBar: true,
				toast: true,
				iconColor: '#ec4706',
				html: "<h1>Credenciales no válidas. Por favor, inténtelo de nuevo.</h1>",
				background: '#454757',
				iconHtml: '
					<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
						<path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
					</svg>
			  	',
				color: 'yellow',
				position: 'top',
				// width: '90%',
				// heigh: '22px',
				timer: 3500
			);
			// session()->flash('msg', __('Invalid credentials. Please try again.'));
		}
	}


	#[Title('Sesión ::')]
    public function render() {
        return view('livewire.access.auth-login');
    }
}
